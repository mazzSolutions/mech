import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mech/ui/dashboard.dart';
import 'package:mech/ui/login_page.dart';
import 'package:mech/upcoming_jobs.dart';

bool locStatus = false;
void main() {
  check();
}

void check() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print("Conneectd");
      runApp(MyApp());
    } else {
      print("Not Conneected ");
    }
  } on SocketException catch (_) {
    print("Not Conneected exception ");
    locStatus = true;
    runApp(MyApp());
    print(locStatus);
    // runApp(Alert());
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // bool widtoggle = toggle;
    // UserLogin user = userLogin;

    return new MaterialApp(
      title: 'Mech',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primaryColor: const Color(0xFF40C4FF),
        primaryColorDark: const Color(0xFF40C4FF),
        accentColor: const Color(0xFF4527A0),
      ),
      // home: locStatus ? Alert() : LoginPage(),
      home: LoginPage(),
      initialRoute: '/',
      routes: {
        // When we navigate to the "/" route, build the FirstScreen Widget

        // When we navigate to the "/second" route, build the SecondScreen Widget
        '/dashboard': (context) => Dashboard(),
        '/upcoming' : (context) => Upcoming(),
        // '/DashBoard' : (context) => UserDashboardScreen(),
      },
    );
  }
}

//        DrawerHeader(
//
//              child: Text('Drawer Header'),
//
//              decoration: BoxDecoration(
//                color: Colors.blue,
//              ),
//            ),
