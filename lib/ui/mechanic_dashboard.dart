import 'package:flutter/material.dart';
import 'package:mech/testwidget.dart';
import 'package:mech/ui/login_page.dart';
import 'package:mech/ui/online_status.dart';
import 'package:mech/user_login.dart';
import '../completed_jobs.dart';
import '../upcoming_jobs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import 'package:mech/globals.dart' as globals;
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import 'profilePage.dart';
import 'package:shared_preferences/shared_preferences.dart';

AudioCache player = new AudioCache();
AudioPlayer audioPlayer;

class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key key, this.title, this.userLogin}) : super(key: key);

  final String title;
  final UserLogin userLogin;

  @override
  _DashboardScreenState createState() => new _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  String userId = "";
  var url = "http://test.mymech.lk//sendData.php";
  String userName = "";
  String email = "";
  final FirebaseMessaging messaging = FirebaseMessaging();
  PageController _pageController;
  int _page = 0;
  List<String> list = [
    "Job Request",
    "Upcoming Jobs",
    "Completed Jobs",
    "My income"
  ];

  @override
  void initState() {
    super.initState();
    sendMobileToken();
    _pageController = new PageController();

    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userName = prefs.getString('userName');
        email = prefs.getString('email');
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void sendMobileToken() async {
     SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString('userId');
      });
    });
    await messaging.getToken().then((token) {
      // print(' this is the $token');

      http.post(url, body: {
        "key": "firebaseToken",
        "token": token,
        "userId": userId,
        "status": "login"
      });
      print(token);
      print(globals.id);
    });
  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          list[_page],
          style: new TextStyle(color: const Color(0xFFFFFFFF)),
        ),
      ),
      drawer: new Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the Drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
//            DrawerHeader(
//
//              child: Text('Drawer Header'),
//
//              decoration: BoxDecoration(
//                color: Colors.blue,
//              ),
//            ),
            UserAccountsDrawerHeader(
              accountName: new Text(userName),
              accountEmail: new Text(email),
              currentAccountPicture: Image.asset("assets/user.png"),
              decoration: BoxDecoration(color: Colors.blueAccent),
            ),
            ListTile(
              title: Text('Profile'),
              leading: new Icon(Icons.person),
              onTap: () {
                // Update the state of the app
                // ...
                Navigator.of(context).pop();
                // // Navigator.pushNamed(context, '/DashBoard');
                Navigator.push(
                  context,
                  //   //MaterialPageRoute(builder: (context) => Profile(userLogin)),
                  MaterialPageRoute(builder: (context) => ProfilePage()),
                );
              },
            ),
            ListTile(
              title: Text('Services'),
              leading: new Icon(Icons.build),
              onTap: () async {
                // Update the state of the app
                // ...
                Navigator.of(context).pop();
                print('kari bijja');
                player.load('song.mp3');
                audioPlayer = await player.play('song.mp3');
                // Navigator.pushNamed(context, '/myService');
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => MyService(userLogin)),
                // );
              },
            ),
            ListTile(
                title: Text('Log Out'),
                onTap: () {
                  // await http.post(url, body: {
                  //   "key": "firebaseToken",
                  //   "token": "0",
                  //   "userId": globals.id,
                  //   "status": "logout"
                  // });
                  // print('logout pressed');
                  // audioPlayer.pause();

                  SharedPreferences.getInstance().then((prefs) {
                    prefs.setString('userName', null);
                  });

                  // Update the state of the app
                  // SharedPreferences.getInstance().then((prefs) {
                  //   prefs.setString('userName', null);
                  // });

                  // // ...
                  // Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
                  // Navigator.pop(context);

                  // Navigator.pushReplacement(context,
                  //     MaterialPageRoute(builder: (context) => LoginPage()));
                  Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
                }),
          ],
        ),
      ),
      body: new PageView(
        children: [
          // new JobRequest("Job Request"),
          // Upcoming(),
          OnlineStatus(),
          // new UpcomingJobs(),
          // new CompletedJobs("Completed Jobs"),
          // new Income("My Income"),
        ],
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: const Color(0xFF1976D2),
        ), // sets the inactive color of the `BottomNavigationBar`
        child: new BottomNavigationBar(
          items: [
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.build,
                  color: const Color(0xFFFFFFFF),
                ),
                title: new Text(
                  "Job Request",
                  style: new TextStyle(
                    color: const Color(0xFFFFFFFF),
                  ),
                )),
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.access_time,
                  color: const Color(0xFFFFFFFF),
                ),
                title: new Text(
                  "Upcoming Jobs",
                  style: new TextStyle(
                    color: const Color(0xFFFFFFFF),
                  ),
                )),
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.check_circle,
                  color: const Color(0xFFFFFFFF),
                ),
                title: new Text(
                  "Completed Jobs",
                  style: new TextStyle(
                    color: const Color(0xFFFFFFFF),
                  ),
                )),
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.attach_money,
                  color: const Color(0xFFFFFFFF),
                ),
                title: new Text(
                  "My Income",
                  style: new TextStyle(
                    color: const Color(0xFFFFFFFF),
                  ),
                ))
          ],
          onTap: navigationTapped,
          currentIndex: _page,
        ),
      ),
    );
  }
}
