import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:mech/globals.dart' as globals;

class OnlineStatus extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OnlineStatusState();
  }
}

class _OnlineStatusState extends State<OnlineStatus> {
  bool _enabled = false;
  String userId = "";
  var url = "http://test.mymech.lk//sendData.php";
  @override
  void initState() {
    // TODO: implement initState
    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString('userId');
      });
    });

    getOnlineState();
    super.initState();
  }

  void getOnlineState() async {
      SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString('userId');
      });
    });
    await http.post(url, body: {
      "key": "onlineState",
      "subKey": "getState",
      "userId": userId,
    }).then((response) {
      String result = response.body;
      print(result);
      if (result == "1") {
        setState(() {
          _enabled = true;
        });
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("You are online"),
        ));
      } else if (result == "0") {
        setState(() {
          _enabled = false;
        });
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("You are offline"),
        ));
      } else {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("Network Error..."),
        ));
      }
    });
  }

  void setOnlineState() async {
    await http.post(url, body: {
      "key": "onlineState",
      "subKey": "updateState",
      "userId": userId,
      "status": _enabled ? "1" : "0",
    }).then((response) {
      String result = response.body;
      if (result == "Record updated successfully") {
        // setState(() {
        //   _enabled = true;
        // });
        _enabled
            ? Scaffold.of(context).showSnackBar(new SnackBar(
                content: new Text("You are online"),
              ))
            : Scaffold.of(context).showSnackBar(SnackBar(
                content: Text("You are offline"),
              ));
        // } else if (result == "0") {
        //   setState(() {
        //     _enabled = false;
        //   });
        //   Scaffold.of(context).showSnackBar(new SnackBar(
        //     content: new Text("You are offline"),
        //   ));
      } else {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("Network Error..."),
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Center(
        child: Transform.scale(
          scale: 3.0,
          child: Switch(
            onChanged: (value) {
              setState(() {
                _enabled = value;
              });
              setOnlineState();
              print(_enabled);
            },
            activeColor: Colors.green,
            inactiveThumbColor: Colors.redAccent,
            value: _enabled,
          ),
        ),
      ),
    );
  }
}
