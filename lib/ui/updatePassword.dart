import 'package:flutter/material.dart';
// import 'package:my_mechanic/ui/google_map.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'updatePassword2.dart';
import 'package:http/http.dart' as http;
import 'mechanic_dashboard.dart';

class PasswordUI extends StatefulWidget {
  @override
  _PasswordUIState createState() => _PasswordUIState();
}

class _PasswordUIState extends State<PasswordUI> {
  var password;
  String userId = "";

  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString("userId");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PASSWORD'),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: ListTile(
              title:
                  Text("Enter new password", style: TextStyle(fontSize: 20.0)),
              subtitle: Text("Update your security details."),
            ),
          ),
          Divider(
            height: 10.0,
          ),
          Container(
            // color: Colors.red,
            child: Row(
              children: <Widget>[
                // TextFormField(
                //   initialValue: "First Name",
                // ),
                // TextFormField(
                //   initialValue: "Last Name",
                // ),
                Expanded(
                  // width: 50.0,
                  // height: 50.0,
                  // color: Colors.green,
                  child: TextField(
                    textAlign: TextAlign.center,
                    controller: controller,
                    // initialValue: "First Name",
                    obscureText: true,
                    decoration: InputDecoration(
                        hintText: "*********",
                        hintStyle: TextStyle(color: Colors.black)),
                    onChanged: (v) {
                      setState(() {
                        password = v;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 20.0,
          ),
          FlatButton(
            color: Colors.black,
            onPressed: () {
              // Navigator.push(context,
              //     MaterialPageRoute(builder: (context) => PasswordUpdate()));
              updateProfile();
            },
            child: Text(
              "Next",
              style: TextStyle(color: Colors.white),
            ),
            padding: EdgeInsets.all(10.0),
          ),
        ],
      ),
    );
  }

  Future<void> _neverSatisfied(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text(message),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Press OK to continue'),
                  //Text('You\’re like me. I’m never satisfied.'),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  //openSettingsMenu("ACTION_SETTINGS");
                  Navigator.of(context).pop();
                  // Navigator.pushNamed(context, '/myCar');
                  Navigator.pushReplacement(
                    context,
                    // MaterialPageRoute(builder: (context) => MyCar(userLogin)),
                    MaterialPageRoute(builder: (context) => DashboardScreen()),
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }

  void updateProfile() async {
    var url = "http://test.mymech.lk//sendData.php";
      SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString('userId');
      });
    });
    await http.post(url, body: {
      "key": "profileChange",
      "subKey": "pass",
      "userId": userId,
      "password":password,
    }).then((response) {
      String result = response.body;
      print(result);
      if (result == "Success") {
        _neverSatisfied("Password Changed!");
      } else {
        _neverSatisfied("Oops! Try again");
      }
    });
  }
}
