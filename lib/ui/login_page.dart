import 'package:flutter/material.dart';
import 'package:mech/ui/dashboard.dart';
import 'package:mech/ui/mechanic_dashboard.dart';
import 'package:mech/user_login.dart';
// import 'package:login/home_page.dart';
import 'package:http/http.dart' as http;
import 'package:mech/utils/colour_loader.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mech/globals.dart' as globals;
import 'dart:convert';

import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String name;
  String email;
  String pass;
  

  @override
  void initState() {
    SharedPreferences.getInstance().then((prefs) {
      if (prefs.getString('userName') != null) {
        UserLogin userLogin = UserLogin(prefs.getString('userName'),
            prefs.getString('email'), prefs.getString('type'));

        // UserLogin userLogin =
        //           UserLogin("Login", "email", "Type");
        // Navigator.pop(context);
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => new Dashboard(
                      // title: 'Bottom Navigation',
                      userLogin: userLogin,
                    )));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool _loadToggle = false;
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/icon.png'),
      ),
    );

    // final email =

    // final password =

    // final loginButton =

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            TextField(
              onChanged: (String value) {
                setState(() {
                  name = value;
                });
              },
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              // initialValue: 'aaaa',
              decoration: InputDecoration(
                hintText: 'User Name',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0)),
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              onChanged: (String value) {
                setState(() {
                  pass = value;
                });
              },
              autofocus: false,
              // initialValue: 'abc',
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Password',
                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0)),
              ),
            ),
            SizedBox(height: 24.0),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                onPressed: () async {
                  print("sdsdsd");
                  // Navigator.of(context).pushNamed(HomePage.tag);

                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ColorLoader()),
                  );

                  var url = "http://test.mymech.lk//sendData.php";

                  await http.post(url, body: {
                    "name": name,
                    "pass": pass,
                    "key": "log",
                  }).then((response) {
                    print("Response body: ${response.body}");
                    List data = (json.decode(response.body));
                    if (data[0] == "error") {
                      Navigator.pop(context);
                    } else {
                      globals.isLoggedIn = true;
                      globals.id = data[0]["id"];
                      globals.type = data[0]["type"];
                      globals.emial = data[0]["email"];
                      globals.userName = data[0]["username"];
                      // globals.fullName = data[0]["fullName"];


                      UserLogin userLogin = UserLogin(
                          globals.userName, globals.emial, globals.type);

                      SharedPreferences.getInstance().then((prefs) {
                        //   // TODO - set logged in user
                        prefs.setString('userId', globals.id);
                        prefs.setString('userName', globals.userName);
                        prefs.setString('email', globals.emial);
                        prefs.setString('type', globals.type);
                        prefs.setBool('loged', true);
                        // prefs.setString("fullName", globals.fullName);
                        

                        if (prefs.getBool('loged') == true)
                          setState(() {
                            _loadToggle = true;
                          });
                      });

                      if (userLogin.getUserName() != null) {
                        if (userLogin.getType() == "M") {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => new Dashboard(
                                      // title: 'Bottom Navigation',
                                      userLogin: userLogin,
                                      )));
                        } else {
                          // showInSnackBar("Login Error");
                          SnackBar(
                            content: AlertDialog(
                              title: Text("error"),
                            ),
                          );
                        }
                      }
                    }
                  });
                },
                padding: EdgeInsets.all(12),
                color: Colors.lightBlueAccent,
                child: Text('Log In', style: TextStyle(color: Colors.white)),
              ),
            ),
            // forgotLabel
          ],
        ),
      ),
    );
  }
}
