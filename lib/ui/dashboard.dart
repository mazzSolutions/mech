import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mech/cancelledJobs.dart';
import 'package:mech/completed_jobs.dart';
import 'package:mech/globals.dart' as globals;
import 'package:mech/ui/complete.dart';
import 'package:mech/ui/profilePage.dart';
import 'package:mech/upcoming_jobs.dart';
import 'package:mech/user_login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

class Dashboard extends StatefulWidget {
  UserLogin userLogin;
  Dashboard({this.userLogin});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DashboardState();
  }
}

class _DashboardState extends State<Dashboard> {
  bool _enabled = false;
  String userId = "";
  var url = "http://test.mymech.lk//sendData.php";

  AudioCache player = new AudioCache();
  AudioPlayer audioPlayer;

  Future playMusic() async {
    player.load('song.mp3');
    audioPlayer = await player.play('song.mp3');
  }

  // Future stopMusic() async {
  //   audioPlayer.pause();
  // }

  @override
  void initState() {
    // TODO: implement initState

    getOnlineState();
    sendMobileToken();
    getAppCount();
    // streamController.stream.listen((data) {
    //   print("DataReceived: " + data);
    // }, onDone: () {
    //   print("Task Done");
    // }, onError: (error) {
    //   print("Some Error");
    // });

    // streamController.add("This a test data");
    // print("code controller is here");

    super.initState();
  }

  // @override
  // void dispose() {
  //   streamController.close(); //Streams must be closed when not needed
  //   super.dispose();
  // }

  int count = 0;
  int newCount = 0;
  Future<String> getAppCount() async {
    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString('userId');
      });
    });
    String url = "http://test.mymech.lk//sendData.php";

    await http.post(url, body: {
      "key": "getappcount",
      "mechid": userId,
    }).then((response) {
      setState(() {
        newCount = int.parse(response.body);
        // toggle = true;
      });
      // if (count < newCount) {
      //   setState(() {
      //     count = newCount;
      //   });
      // }
      print(newCount);
    });
  }

  // StreamController<String> streamController = new StreamController();

  Stream _streamRequest() async* {
    String url = "http://test.mymech.lk//sendData.php";

    yield* Stream.periodic(Duration(seconds: 10), (int a) {
      // print(a++);

      getAppCount();
    });
  }

  Material Item(IconData icon, String heading, Color color) {
    return Material(
      color: Colors.white,
      elevation: 14.0,
      shadowColor: Colors.grey,
      borderRadius: BorderRadius.circular(24.0),
      child: InkWell(
        onTap: () {
          // print(heading);
          switch (heading) {
            case "Upcoming\nService":
              // print(heading);
              if (newCount > count) {
                setState(() {
                  count = newCount;
                });
                // stopMusic();
              }
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Upcoming()));
              break;
            case "Completed\nService":
              // print(heading);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Complete()));
              break;
            case "Cancelled\nService":
              // print(heading);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CancelledJobs()));
              break;
            case "Emergency":
              // print(heading);
              launch("tel://+94783589091");
              break;
            default:
          }
        },
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        heading,
                        style: TextStyle(color: color, fontSize: 20.0),
                      ),
                    ),
                    heading == "Status"
                        ? Transform.scale(
                            scale: 2,
                            child: Switch(
                              onChanged: (value) {
                                setState(() {
                                  _enabled = value;
                                });
                                setOnlineState();
                                print(_enabled);
                              },
                              activeColor: Colors.green,
                              inactiveThumbColor: Colors.redAccent,
                              value: _enabled,
                            ),
                          )
                        : Material(
                            color: color,
                            borderRadius: BorderRadius.circular(24),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Icon(
                                icon,
                                color: Colors.white,
                                size: 30.0,
                              ),
                            ),
                          )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  

  final FirebaseMessaging messaging = FirebaseMessaging();

  void sendMobileToken() async {
    await messaging.getToken().then((token) {
      SharedPreferences.getInstance().then((prefs) {
        http.post(url, body: {
          "key": "firebaseToken",
          "token": token,
          "userId": prefs.getString('userId'),
          "status": "login"
        });
        print(token);
        print(globals.id);
      });
    });
  }

  void getOnlineState() async {
    SharedPreferences.getInstance().then((prefs) {
      http.post(url, body: {
        "key": "onlineState",
        "subKey": "getState",
        "userId": prefs.getString('userId'),
      }).then((response) {
        String result = response.body;
        print(result);
        if (result == "1") {
          setState(() {
            _enabled = true;
          });
          // Scaffold.of(context).showSnackBar(new SnackBar(
          //   content: new Text("You are online"),
          // ));
        } else if (result == "0") {
          setState(() {
            _enabled = false;
          });
          // Scaffold.of(context).showSnackBar(new SnackBar(
          //   content: new Text("You are offline"),
          // ));
        }
        // else {
        // Scaffold.of(context).showSnackBar(new SnackBar(
        //   content: new Text("Network Error..."),
        // ));
        // }
      });
    });
  }

  void setOnlineState() async {
    await http.post(url, body: {
      "key": "onlineState",
      "subKey": "updateState",
      "userId": userId,
      "status": _enabled ? "1" : "0",
    });
    // .then((response) {
    //   String result = response.body;
    //   if (result == "Record updated successfully") {
    //     // setState(() {
    //     //   _enabled = true;
    //     // });
    //     _enabled
    //         ? Scaffold.of(context).showSnackBar(new SnackBar(
    //             content: new Text("You are online"),
    //           ))
    //         : Scaffold.of(context).showSnackBar(SnackBar(
    //             content: Text("You are offline"),
    //           ));
    //     // } else if (result == "0") {
    //     //   setState(() {
    //     //     _enabled = false;
    //     //   });
    //     //   Scaffold.of(context).showSnackBar(new SnackBar(
    //     //     content: new Text("You are offline"),
    //     //   ));
    //   } else {
    //     Scaffold.of(context).showSnackBar(new SnackBar(
    //       content: new Text("Network Error..."),
    //     ));
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(    
        appBar: AppBar(
          title: Text("Your Status"),
        ),
        drawer: new Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: new Text(widget.userLogin.getUserName()),
                accountEmail: new Text(widget.userLogin.getEmail()),
                currentAccountPicture: Image.asset("assets/user.png"),
                decoration: BoxDecoration(color: Colors.blueAccent),
              ),
              ListTile(
                title: Text('Profile'),
                leading: new Icon(Icons.person),
                onTap: () {
                  Navigator.of(context).pop();

                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ProfilePage()),
                  );
                },
              ),
              // ListTile(
              //   title: Text('Services'),
              //   leading: new Icon(Icons.build),
              //   onTap: () async {
              //     // Update the state of the app
              //     // ...
              //     Navigator.of(context).pop();
              //     print('kari bijja');
              //     // player.load('song.mp3');
              //     // audioPlayer = await player.play('song.mp3');

              //   },
              // ),

              ListTile(
                  title: Text('Log Out'),
                  onTap: () {
                    // await http.post(url, body: {
                    //   "key": "firebaseToken",
                    //   "token": "0",
                    //   "userId": globals.id,
                    //   "status": "logout"
                    // });
                    // print('logout pressed');
                    // audioPlayer.pause();

                    SharedPreferences.getInstance().then((prefs) {
                      prefs.setString('userName', null);
                    });

                    // Update the state of the app
                    // SharedPreferences.getInstance().then((prefs) {
                    //   prefs.setString('userName', null);
                    // });

                    // // ...
                    // Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
                    // Navigator.pop(context);

                    // Navigator.pushReplacement(context,
                    //     MaterialPageRoute(builder: (context) => LoginPage()));
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/', (_) => false);
                  }),
            ],
          ),
        ),
        body: StreamBuilder<Object>(
            stream: _streamRequest(),
            initialData: count,
            builder: (context, snapshot) {
              // if (newCount > count) {
              //   playMusic();
              // }
              return StaggeredGridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                children: <Widget>[
                  Item(Icons.time_to_leave, "Status", Colors.black),
                  Item(Icons.access_time, "Upcoming\nService",
                      newCount > count ? Colors.green : Colors.grey),
                  Item(Icons.done_all, "Completed\nService", Colors.indigo),
                  Item(Icons.cancel, "Cancelled\nService", Colors.lightBlue),
                  Item(Icons.call, "Emergency", Colors.redAccent),
                  // Item(Icons., heading, color)
                ],
                staggeredTiles: [
                  StaggeredTile.extent(2, 130.0),
                  StaggeredTile.extent(1, 150.0),
                  StaggeredTile.extent(1, 150.0),
                  StaggeredTile.extent(1, 150.0),
                  StaggeredTile.extent(1, 150.0),
                ],
              );
            })
        // body: Center(
        //   child: Transform.scale(
        //     scale: 3.0,
        //     child: Switch(
        //       onChanged: (value) {
        //         setState(() {
        //           _enabled = value;
        //         });
        //         setOnlineState();
        //         print(_enabled);
        //       },
        //       activeColor: Colors.green,
        //       inactiveThumbColor: Colors.redAccent,
        //       value: _enabled,
        //     ),
        //   ),
        // ),
        );
  }
}
