import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mech/globals.dart' as globals;

class AppointmentDetails extends StatefulWidget {
  // if (timeSlot == null) timeSlot = 1;
  dynamic list;
  bool raiseButton;
  AppointmentDetails({this.list, bool raiseButton = false}) {
    // print(list["fname"]);
    this.raiseButton = raiseButton;
  }

  @override
  _AppointmentDetailsState createState() => _AppointmentDetailsState();
}

class _AppointmentDetailsState extends State<AppointmentDetails> {
  bool toggle = true;

  Future<void> _neverSatisfied(message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text(message),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Press OK to continue'),
                  //Text('You\’re like me. I’m never satisfied.'),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  
                  //openSettingsMenu("ACTION_SETTINGS");
                  Navigator.pop(context);
                  Navigator.pop(context);
                  // _Satisfied();
                  
                  // Navigator.pushNamedAndRemoveUntil(
                  //     context, '/upcoming', (_) => false);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  var url = "http://test.mymech.lk//sendData.php";

  // void _sendNotification() async {
  //   await http.post(url, body: {
  //     "token": widget.deviceToken,
  //     // "pass": password,
  //     "key": "request",
  //   });
  // }

  void _completeService() async {
    await http.post(url, body: {
      "app_id": widget.list["appointment_id"],
      "key": "complete_app",
    }).then((response){
      List result=json.decode(response.body);
       if (result[0]["error"] == "Wrong") {
        // _sendNotification();
        _neverSatisfied('Oops! Network Error');
      } else {
        _neverSatisfied("Congrats");
      }
    });
  }

  void _makeBooking() async {
    await http.post(url, body: {
      // "key": "makeAppointment",
      // "userId": widget.userId,
      // "lat": widget.lat.toString(),
      // "lon": widget.lan.toString(),
      // "userNote": userNote,
      // "mechId": widget.mechId,
      // "price": widget.price.substring(11, 18),
      // "vehicleSeriveId": widget.vehicleServiceType,
      // "service_Type": widget.service,
      // "vehicleId": widget.carId,
      // "groupvalue": widget.timeSlot.toString(),
      // "date": widget.date.toString(),
      // "visitType": widget.visit.toString(),
    }).then((respose) {
      String status = respose.body;
      print(status);
      if (status == "Success") {
        // _sendNotification();
        _neverSatisfied('Service added successfully');
      } else {
        _neverSatisfied("Service added failed");
      }
    });
  }

  String time = "";

  @override
  Widget build(BuildContext context) {
    if (widget.list["appointment_slot"] == 1)
      time = "8 am-10 am";
    else if (widget.list["appointment_slot"] == 2)
      time = "10 am-12 pm";
    else if (widget.list["appointment_slot"] == 3)
      time = "12 am-2 pm";
    else
      time = "Emergency";

    return Scaffold(
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 200.0,
            floating: true,
            pinned: false,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                widget.list["service_name"],
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              background: Image.network(
                "https://media.istockphoto.com/photos/auto-mechanic-service-and-repair-picture-id652660058?k=6&m=652660058&s=612x612&w=0&h=kaPNUKxm6-DVr_OEs5fOjeXpe5hfESqc-NLwKVHswek=",
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverList(
              delegate: SliverChildBuilderDelegate(
            (context, index) {
              return Container(
                child: Column(
                  children: <Widget>[
                    SafeArea(
                      child: SizedBox(
                        child: Container(
                          child: Card(
                            margin: EdgeInsets.all(20.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  leading: Container(
                                    padding: EdgeInsets.only(right: 12.0),
                                    decoration: new BoxDecoration(
                                        border: new Border(
                                            right: new BorderSide(
                                                width: 1.0,
                                                color: Colors.white24))),
                                    child: Icon(
                                      Icons.person,
                                      color: Colors.blue,
                                      size: 50,
                                    ),
                                  ),
                                  title: Text("Name"),
                                  subtitle: Text(widget.list["fname"]),
                                ),
                                Divider(
                                  color: Colors.black,
                                ),
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  title: Text("User Note"),
                                  subtitle: Text(widget.list["user_note"]),
                                ),
                                // ListTile(
                                //   contentPadding: EdgeInsets.symmetric(
                                //       horizontal: 20.0, vertical: 10.0),
                                //   title: Text("Job completed(7)"),
                                // ),
                                // ListTile(
                                //   contentPadding: EdgeInsets.symmetric(
                                //       horizontal: 20.0, vertical: 10.0),
                                //   title: Text("Job completed(7)"),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   child: Container(
                    //     child: Card(
                    //       margin: EdgeInsets.all(20.0),
                    //       child: Column(
                    //         children: <Widget>[
                    //           GoogleMap(
                    //             initialCameraPosition: CameraPosition(
                    //                 target: LatLng(6.029299, 80.215142)),
                    //           ),
                    //           Divider(
                    //             color: Colors.black,
                    //           ),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      child: Container(
                        child: Card(
                          margin: EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                title: Text("Car Details"),
                              ),
                              Divider(
                                color: Colors.black,
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Icon(
                                    Icons.directions_car,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                ),
                                title: Text("Car Brand"),
                                subtitle: Text(widget.list["brand_name"]),
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Icon(
                                    Icons.texture,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                ),
                                title: Text("Car Model"),
                                subtitle:
                                    Text(widget.list["vehicle_model_name"]),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Container(
                        child: Card(
                          margin: EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                title: Text("Appointment Details "),
                              ),
                              Divider(
                                color: Colors.black,
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Icon(
                                    Icons.calendar_today,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                ),
                                title: Text("Date"),
                                subtitle: Text(
                                    widget.list["appointment_date"].toString()),
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Icon(
                                    Icons.timelapse,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                ),
                                title: Text("Time"),
                                subtitle: Text(time),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: widget.raiseButton
                          ? RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(30.0)),
                              padding: const EdgeInsets.only(
                                left: 70.0,
                                right: 70.0,
                              ),
                              elevation: 10.0,
                              onPressed: () {
                                setState(() {
                                  toggle = false;
                                });
                                _completeService();
                                // http.post(url, body: {
                                //   "token": widget.deviceToken,
                                //   // "pass": password,
                                //   "key": "request",
                                // });
                                // _makeBooking();
                                // _neverSatisfied();
                              },
                              color: toggle ? Colors.green : Colors.grey,
                              child: Text(
                                "Service Completed",
                                style: TextStyle(color: Colors.white),
                              ),
                            )
                          : Container(),
                    ),
                  ],
                ),
              );
            },
            childCount: 1,
          ))
        ],
      ),
    );
  }
}
