import 'package:flutter/material.dart';

class Alert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Center(
        child: AlertDialog(
          title: Text("Location Update Failed"),
          content: Text("Please turn on location"),
          actions: <Widget>[
            FlatButton(
              child: Text("Settings"),
              onPressed: (){
                
              },
            )
          ],
        ),
      ),
    );
  }
}
