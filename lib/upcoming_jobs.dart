import 'dart:convert';
import 'package:mech/globals.dart' as globals;

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:mech/ui/appointment_details.dart';
import 'package:mech/ui/dashboard.dart';
import 'package:mech/ui/mechanic_services.dart';

import 'package:shared_preferences/shared_preferences.dart';

class Upcoming extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UpcomingState();
  }
}

class _UpcomingState extends State<Upcoming> {
  String usrId = "";

  bool toggle = false;
  var url = "http://test.mymech.lk//sendData.php";

  List data;
  Future<String> makeRequest() async {
    SharedPreferences.getInstance().then((prefs) {
      http.post(url, body: {
        "key": "myAppo",
        "user_id": prefs.getString('userId'),
        "subKey": "0",
      }).then((response) {
        // print(response.body);
        if (mounted) {
          setState(() {
            data = json.decode(response.body);
            toggle = true;
          });
        }
        print(data);
      });
    });
  }

  @override
  void initState() {
    // if (prefs.getString('userName') != null) {
    //   UserLogin userLogin = UserLogin(prefs.getString('userName'),
    //       prefs.getString('email'), prefs.getString('type'));}
    this.makeRequest();
    super.initState();
  }

  void _deleteItem(String appId) async {
    await http.post(url, body: {
      "key": "cancel_app",
      "app_id": appId,
    }).then((response) {
      print(response.body);
      List result = json.decode(response.body);
      if (result[0]["error"] == "Wrong") {
        _neverSatisfied("Cannot delete at the moment");
      }
    });
  }

  Future<void> _neverSatisfied(String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text(message),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Press OK to continue'),
                  //Text('You\’re like me. I’m never satisfied.'),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  //
                  //openSettingsMenu("ACTION_SETTINGS");
                  Navigator.of(context).pop();
                  // Navigator.pushNamed(context, '/myCar');
                  Navigator.push(
                    context,
                    // MaterialPageRoute(builder: (context) => MyCar(userLogin)),
                    MaterialPageRoute(builder: (context) => Dashboard()),
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Upcoming Services"),
      ),
      body: toggle
          ? data[0]["error"] != "Wrong"
              ? new ListView.builder(
                  itemCount: data[0]["error"] == "Wrong" ? 0 : data.length,
                  // itemExtent: 100.0,
                  itemBuilder: (BuildContext context, i) {
                    return Dismissible(
                        key: Key(data.toString()),
                        background: Container(
                          color: Colors.red[300],
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(16.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.all(4),
                              ),
                              Text(
                                "Delete",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                        ),
                        onDismissed: (direction) {
                          data.removeAt(i);
                          Scaffold.of(context).showSnackBar(new SnackBar(
                            content: new Text("item remoed"),
                          ));
                          _deleteItem(data[i]["appointment_id"]);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Container(
                            // child: FittedBox(
                            child: Material(
                              color: Colors.white,
                              elevation: 14.0,
                              borderRadius: BorderRadius.circular(5.0),
                              shadowColor: Colors.grey,
                              child: ListTile(
                                title: Text(data[i]["service_name"]),
                                subtitle: Text(data[i]["vehicle_model_name"]),
                                trailing: IconButton(
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              MechanicServices(appoId: data[i]["appointment_id"],))),
                                  icon: Icon(Icons.library_add),
                                  color: Colors.green,
                                  iconSize: 50,
                                ),
                                onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            AppointmentDetails(
                                              list: data[i],
                                              raiseButton: true,
                                            ))),
                              ),
                            ),
                            // ),
                          ),
                        ));
                  })
              : Container(
                  child: Center(
                    child: Text("No Upcoming Services"),
                  ),
                )
          : SpinKitFadingCircle(
              color: Colors.blueAccent,
              size: 50,
            ),
      // body: Center(
      //   child: Text("SSS"),
      // ),
    );
  }
}
