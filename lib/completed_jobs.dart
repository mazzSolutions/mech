import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mech/globals.dart' as globals;

class CompletedJobs extends StatefulWidget {
  @override
  _CompletedJobsState createState() => _CompletedJobsState();
}

class _CompletedJobsState extends State<CompletedJobs> {
  List data = [];
  String userId = "";

  @override
  void initState() {
    // streamController = StreamController.broadcast();

    // streamController.stream.listen((p) => setState(() => list.add(p)));

    // load(streamController);
    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString("userId");
      });
    });

    makeRequest();

    super.initState();
  }

  bool toggle = false;
  var url = "http://test.mymech.lk//sendData.php";
  List completeList = [];

  // List data;
  Future<String> makeRequest() async {
     SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString('userId');
      });
    });
    await http.post(url,
        body: {"key": "myAppo", "user_id": userId, "subKey": "1"}).then((respose) {
      print(respose.body);
      setState(() {
        completeList = json.decode(respose.body);
        toggle = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: toggle
            ? data[0]["error"] != "Wrong"
                ? new ListView.builder(
                    itemCount:
                        data[0]["error"] != "Wrong" ? 0 : completeList.length,
                    // itemExtent: 100.0,
                    itemBuilder: (BuildContext context, i) {
                      return Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Container(
                          // child: FittedBox(
                          child: Material(
                            color: Colors.white,
                            elevation: 14.0,
                            borderRadius: BorderRadius.circular(5.0),
                            shadowColor: Colors.grey,
                            child: ListTile(
                              title: Text(data[i]["Name"]),
                              subtitle: Text(data[i]["Name"]),
                              onTap: () {},
                            ),
                          ),
                        ),
                      );
                    },
                  )
                : Container(
                    child: Center(
                      child: Text("No any completed jobs"),
                    ),
                  )
            : SpinKitCircle(
                color: Colors.blue,
              ));
  }
}
