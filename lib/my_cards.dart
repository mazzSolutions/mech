import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyCard{
  MyCard();
Container makeBody(){ 
  
  int id=1;
  return Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return makeCard(id++);
        },
      ),
    );
}

Card makeCard (int id){
   
  // id++;
  return  Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Colors.white10),
        child: makeListTile(id),
        
      ),
    );
}

ListTile makeListTile (int id){ 
 
  return ListTile(
    
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: new BoxDecoration(
              border: new Border(
                  right: new BorderSide(width: 1.0, color: Colors.white24))),
          child: Icon(Icons.done_outline, color: Colors.blue),
        ),
        title: Text(
          "JOB ID :  $id",
          style: TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.bold),
        ),
        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

        subtitle: Row(
          children: <Widget>[
            Icon(Icons.linear_scale, color: Colors.blue),
            Text(" Customer Rating", style: TextStyle(color: Colors.blueGrey)),
            

          ],
          
        ),
        trailing:
            Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0));
}
}

